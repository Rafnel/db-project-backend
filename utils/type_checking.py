# checks if a string value is of a certain domain (int, float, boolean, string)
def check_value_follows_domain(value, domain):
    if domain == "string":
        # string allows any type of value, so we are good.
        return True
    elif domain == "integer":
        if isinstance(value, int) or is_int(value):
            return True
        else:
            return False
    elif domain == "floating_point":
        if isinstance(value, (float, int)) or is_float(value):
            return True
        else:
            return False
    elif domain == "boolean":
        if isinstance(value, str) and value.lower() == "t" or value.lower() == "f" or \
                value.lower() == "true" or value.lower() == "false" or value.lower() == "0" or value.lower() == "1":
            return True
        else:
            return False


def is_int(value):
    if value[0] == '-':
        return value[1:].isdigit()
    else:
        return value.isdigit()


def is_float(value):
    try:
        float(value)
        return True
    except ValueError:
        return False
