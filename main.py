from flask import Flask
from sql import createTables
from data_input_flask_endpoints.csv_input import csv_input
from data_input_flask_endpoints.input_fields import field_input
from data_input_flask_endpoints.manual_input import manual_input
from data_input_flask_endpoints.get_experiments import get_experiments
from flask_cors import CORS

from sql.connection import establish_db_connection

app = Flask(__name__)
# register the csv_input file as part of the application
app.register_blueprint(csv_input)
app.register_blueprint(field_input)
app.register_blueprint(manual_input)
app.register_blueprint(get_experiments)
# enable CORS requests for our application
CORS(app)


@app.route('/', methods=['GET', 'POST'])
def test():
    # Switch to this connector for local
    # mydb = mysql.connector.connect(host="localhost", user="root", passwd="", database="")
    # Switch to this connector for remote
    mydb = establish_db_connection()
    cursor = mydb.cursor()
    # Create Tables (if needed)
    createTables.createAllTables(mydb)

    # cursor.execute("SHOW TABLES")
    # output_string = "Existing Tables: "
    # for tables in cursor.fetchall():
    #     output_string += (" " + tables[0] + " | ")
    # return output_string
    output_string = "<a href=\"http://db-project-frontend.herokuapp.com/\">Frontend Website</a>"    return output_string


if __name__ == '__main__':
    app.run()
