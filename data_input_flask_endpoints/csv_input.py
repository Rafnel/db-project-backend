from flask import Blueprint, request
from sql.connection import establish_db_connection
from sql.input import input_new_experiment, input_new_experiment_with_condition, input_new_measurement_with_experiment, \
    input_new_measurement_with_experiment_different_return
from sql.retrieval import get_sequence, get_condition, get_measurement, get_experiment_with_conditions, \
    get_all_experiments
from utils.type_checking import check_value_follows_domain

# register this python file as part of the flask app.
csv_input = Blueprint("csv_input", __name__)

@csv_input.route("/import-csv", methods=["POST"])
def input_csv_data():
    response_code = 201
    # object to tell the frontend user what was entered, what wasn't entered, etc.
    response_object = {
        "invalid_experiments": [],
        "experiments_created": [],
        "invalid_measurements": [],
        "measurement_values_created": []
    }
    csv_data = request.get_json()["data"]

    if len(csv_data) == 0:
        response_object["invalid_experiments"].append({
            "experiment_name": "CSV File",
            "reason": "Your CSV file is empty. Please put data in your file."
        })
        return response_object, response_code

    mydb = establish_db_connection()
    # 1: handle the first row. Each cell in this row, aside from the first cell, has a new experiment to add.
    # remove the useless first cell
    csv_data[0].pop(0)
    # send to helper function
    inserted_experiment_ids = []
    enter_new_experiments(csv_data[0], response_object, inserted_experiment_ids, mydb)

    measurements = csv_data[1:]
    # 2: handle all subsequent rows, each of those is a measurement with the potential measurement value associated
    # with the experiment.
    enter_measurements_with_experiments(inserted_experiment_ids, measurements, response_object, mydb)

    return response_object, response_code


def enter_new_experiments(experiments, response_object, inserted_experiment_ids, mydb):
    # for each proposed experiment...
    for experiment in experiments:
        experiment_features = experiment.split('_')
        # 1. ensure that the sequence for this experiment exists.
        if get_sequence(experiment_features[0], mydb) is None:
            # the sequence doesn't exist, so skip this experiment and add it to the list of invalid ones.
            response_object["invalid_experiments"].append({
                "experiment_name": experiment,
                "reason": "Sequence " + experiment_features[0] + " does not exist in the database."
            })
            inserted_experiment_ids.append(experiment)
            continue

        sequence_name = experiment_features.pop(0)
        # 2. ensure that each condition in this experiment exists, and that its value matches its domain.
        # AND 3. Ensure that this set of (sequence, conditions+values) does not already exist as an experiment.
        i = 0
        bad_experiment = False
        all_exist_already = True
        while i < len(experiment_features):
            condition = get_condition(experiment_features[i], mydb)
            if condition is None:
                # this condition doesn't exist, so document it and break out of this loop
                bad_experiment = True
                response_object["invalid_experiments"].append({
                    "experiment_name": experiment,
                    "reason": "Condition " + experiment_features[i] + " does not exist in the database."
                })
                inserted_experiment_ids.append(experiment)
                break
            else:
                # this condition exists, ensure that the value is of the right domain.
                follows_domain = check_value_follows_domain(experiment_features[i + 1], condition[1])
                if not follows_domain:
                    # the value doesn't follow the domain.. so we can't enter this experiment.
                    bad_experiment = True
                    response_object["invalid_experiments"].append({
                        "experiment_name": experiment,
                        "reason": "Condition " + experiment_features[i] + " with value " + experiment_features[i + 1] +
                                  " doesn't follow the condition domain of " + condition[1]
                    })
                    inserted_experiment_ids.append(experiment)
                    all_exist_already = False
                    break

                # if this condition is a boolean, change it to true or false for consistency.
                if condition[1] == "boolean":
                    if experiment_features[i + 1].lower().startswith("t") or experiment_features[i + 1] == "1":
                        experiment_features[i + 1] = "true"
                    else:
                        experiment_features[i + 1] = "false"

                # now see if this exact (sequence, condition, condition_value) exist already.
                experiment_with_conditions = get_experiment_with_conditions(experiment_features[i],
                                                                            experiment_features[i + 1], mydb)
                if experiment_with_conditions is None:
                    all_exist_already = False
                else:
                    # there are some exp_with_conds with same cond_name and cond_value, check if same seq_name.
                    one_matches = False
                    for exp_with_cond in experiment_with_conditions:
                        split_list = exp_with_cond[1].split("_")
                        if split_list[0] == sequence_name:
                            one_matches = True
                    if not one_matches:
                        all_exist_already = False

            i = i + 2

        if all_exist_already:
            response_object["invalid_experiments"].append({
                "experiment_name": experiment,
                "reason": "Experiment " + experiment + " already exists in the database."
            })
            inserted_experiment_ids.append(experiment)
            continue

        if bad_experiment:
            continue

        # okay, we made it here, this means that this experiment has passed all of our checks. We need to create this
        # new experiment in the database and also create all of the entries for experiment_with_condition
        # 1. enter new experiment in the experiments table
        insert_experiment_result = input_new_experiment(experiment, sequence_name, mydb)
        if insert_experiment_result is False:
            # insertion failed for an unknown reason.
            response_object["invalid_experiments"].append({
                "experiment_name": experiment,
                "reason": "Could not insert this experiment. Is it possible that your sequence doesn't exist?"
            })
            inserted_experiment_ids.append(experiment)
            continue

        # 2. enter each of the condition values in new tuples in the experiment_with_condition table.
        i = 0
        while i < len(experiment_features):
            result = input_new_experiment_with_condition(experiment_features[i], experiment,
                                                         experiment_features[i + 1], mydb)
            i = i + 2

        response_object["experiments_created"].append(experiment)
        inserted_experiment_ids.append(experiment)


def enter_measurements_with_experiments(experiment_ids, measurement_list, response_object, mydb):
    # for each row in the csv after the first row, (each row is a bunch of measurement values)
    for measurement in measurement_list:
        # 1: check that the measurement actually exists in the database.
        measurement_name = measurement[0]
        if measurement_name == "":
            continue
        measurement_from_db = get_measurement(measurement_name, mydb)
        if measurement_from_db is None:
            response_object["invalid_measurements"].append({
                "measurement": measurement[0],
                "reason": "Measurement " + measurement[0] + " doesn't exist in the database."
            })
            continue

        # so we now know that the measurement exists in the database.
        measurement.pop(0)
        # for each measurement value for this measurement
        i = 0
        while i < len(measurement):
            # check that the value isn't empty.
            if measurement[i] == "":
                i = i + 1
                continue
            # check that the value corresponds with the domain for the measurement.
            follows_domain = check_value_follows_domain(measurement[i], measurement_from_db[1])
            if not follows_domain:
                response_object["invalid_measurements"].append({
                    "measurement": measurement[i],
                    "reason": "Measurement value " + measurement[i] + " for measurement " + measurement_name +
                              " doesn't match the domain of: " + measurement_from_db[1]
                })
                i = i + 1
                continue

            # if we made it here, the measurement exists, the value follows the domain and isn't empty, and the
            # experiment to tie the value with also exists. insert this measurement_with_experiment.
            result = input_new_measurement_with_experiment_different_return(measurement_name, experiment_ids[i], measurement[i], mydb)
            if result["status"] is False:
                if "Duplicate" in result["message"]:
                    response_object["invalid_measurements"].append({
                        "measurement": measurement[i],
                        "reason": "Measurement " + measurement_name + " has a value already recorded for experiment " + experiment_ids[i] + "."
                    })
                else:
                    response_object["invalid_measurements"].append({
                        "measurement": measurement[i],
                        "reason": "Measurement " + measurement_name + " with value " + measurement[i] + " can not be inserted for experiment " + experiment_ids[i] + " because this experiment doesn't exist."
                    })
            else:
                response_object["measurement_values_created"].append({
                    "measurement": measurement_name,
                    "measurement_value": measurement[i],
                    "experiment_id": experiment_ids[i]
                })
            i = i + 1
