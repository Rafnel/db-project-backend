from flask import Blueprint, request
from sql.get_Table import get_experiments_with_sequence, get_condtions_with_experiment_id, \
    get_measurements_with_experiment_id

# register this python file as part of the flask app.
from sql.connection import establish_db_connection
from sql.input import input_new_sequence
from sql.retrieval import get_all_conditions, get_all_sequences, get_all_experiments, \
    get_condition_values_with_name, get_all_measurements

get_experiments = Blueprint("get_experiments", __name__)


# Returns all experiments that match a given sequence and its conditions.
@get_experiments.route("/get-experiments", methods=["GET"])
def get_sequences_opt1():
    return_list = []
    mydb = establish_db_connection()

    given_sequence_name = request.args['sequence_name']  # this sends a 400 error if we don't get anything
    given_condition_names = request.args.getlist('condition_names[]')
    given_condition_values = request.args.getlist('condition_values[]')

    # We throw a fit if we don't get the same number of condition elements
    assert (len(given_condition_values) == len(given_condition_names))

    merged_list = [(given_condition_names[i], given_condition_values[i]) for i in range(0, len(given_condition_names))]

    experiments = get_experiments_with_sequence(given_sequence_name, mydb)

    # For each experiment that has the given sequence we get the experiment's conditions and measurements.
    # If the condition list gotten via query matches the provided list, we store the experiment in JSON list return_list
    for experiment_id in experiments:
        conditions = get_condtions_with_experiment_id(experiment_id[0], mydb)
        measurements = get_measurements_with_experiment_id(experiment_id[0], mydb)

        # This sees if the conditions in the experiment match the provided ones.
        # if so, we return the experiment.

        # checks if the conditions provided are all in the experiment
        # checks if the provided conditions are empty.
        if (set(merged_list).issubset(set(conditions))) or (given_condition_names[0] == '' and given_condition_values[0] == ''):

            experiment_request_object = {
                "experiment_id": experiment_id[0],
                "measurements": measurements,
                "conditions": conditions
            }
            return_list.append(experiment_request_object)


    request_object = {
        "experiments": return_list
    }

    return request_object


@get_experiments.route("/get-all-conditions", methods=["GET"])
def retrieve_all_conditions():
    return_list = []
    mydb = establish_db_connection()

    return_list = get_all_conditions(mydb)
    return_object = {
        "conditions": return_list
    }

    return return_object


@get_experiments.route("/get-condition-values-with-name", methods=["GET"])
def retrieve_experiment_values_with_name():
    mydb = establish_db_connection()
    condition_name = request.args["condition_name"]

    return_list = get_condition_values_with_name(condition_name, mydb)

    return_object = {
        "conditions": return_list
    }

    return return_object


@get_experiments.route("/compare-two-experiments", methods=["GET"])
def compare_two_experiments():
    return_list = []
    measurements_1_result = []
    measurements_2_result = []

    mydb = establish_db_connection()

    experiment_one_id = request.args['experiment_1_id']  # this sends a 400 error if we don't get anything
    experiment_two_id = request.args['experiment_2_id']  # this sends a 400 error if we don't get anything

    measurements_1 = get_measurements_with_experiment_id(experiment_one_id, mydb)
    measurements_2 = get_measurements_with_experiment_id(experiment_two_id, mydb)

    conditions_1 = get_condtions_with_experiment_id(experiment_one_id, mydb)
    conditions_2 = get_condtions_with_experiment_id(experiment_two_id, mydb)

    measurement_1_names = [i[0] for i in measurements_1]
    measurement_2_names = [i[0] for i in measurements_2]

    intersect = set(measurement_1_names).intersection(set(measurement_2_names))

    for tuple in measurements_1:
        if tuple[0] in intersect:
            measurements_1_result.append(tuple)

    for tuple in measurements_2:
        if tuple[0] in intersect:
            measurements_2_result.append(tuple)

    experiment_request_object = {
        "experiment_id": experiment_one_id,
        "measurements": measurements_1_result,
        "conditions": conditions_1
    }
    return_list.append(experiment_request_object)

    experiment_request_object2 = {
        "experiment_id": experiment_two_id,
        "measurements": measurements_2_result,
        "conditions": conditions_2
    }
    return_list.append(experiment_request_object2)

    request_object = {
        "experiments": return_list
    }

    return request_object


@get_experiments.route("/get-all-experiments", methods=["GET"])
def retrieve_all_experiments():
    return_list = []
    mydb = establish_db_connection()

    experiments = get_all_experiments(mydb)

    for experiment_id in experiments:
        return_list.append(experiment_id[0])

    request_object = {
        "experiments": return_list
    }
    return request_object


@get_experiments.route("/get-all-sequences", methods=["GET"])
def retrieve_all_sequences():
    return_list = []
    mydb = establish_db_connection()

    return_list = get_all_sequences(mydb)
    return_object = {
        "sequences": return_list
    }

    return return_object


@get_experiments.route("/get-all-measurements", methods=["GET"])
def retrieve_all_measurements():
    return_list = []
    mydb = establish_db_connection()

    return_list = get_all_measurements(mydb)
    return_object = {
        "measurements": return_list
    }

    return return_object


# Returns all experiments that match any provided sequence and any provided condition
@get_experiments.route("/get-measurements-from-experiments", methods=["GET"])
def get_measurements_opt_extracred():
    return_list = []
    mydb = establish_db_connection()
    given_experiment_ids = request.args.getlist('experiment_ids[]') # this sends a 400 error if we don't get anything
    given_measurement_names = request.args.getlist('measurement_names[]')  # this sends a 400 error if we don't get anything

    for experiment_id in given_experiment_ids:
        measurements = get_measurements_with_experiment_id(experiment_id, mydb)
        measurementnames = [i[0] for i in measurements]
        measurement_list = []
        for measurement in given_measurement_names:
            if measurement in measurementnames:
                measurement_object = {
                    "name": measurement,
                    "value": measurements[measurementnames.index(measurement)][1]
                }
            else:
                measurement_object = {
                    "name": measurement,
                    "value": "-"
                }
            measurement_list.append(measurement_object)

        # lists must have at least one item in common
        experiment_request_object = {
            "experiment_id": experiment_id,
            "measurements": measurement_list,
        }
        return_list.append(experiment_request_object)

    request_object = {
        "experiments": return_list
    }

    return request_object


# Returns all experiments that match any provided sequence and any provided condition
@get_experiments.route("/get-extra-cred-experiments-info", methods=["GET"])
def get_experiments_opt_extra_cred():
    return_list = []
    mydb = establish_db_connection()

    given_sequence_names = request.args.getlist('sequence_names[]')  # this sends a 400 error if we don't get anything
    given_condition_names = request.args.getlist('condition_names[]')  # this sends a 400 error if we don't get anything
    given_condition_values = request.args.getlist('condition_values[]')  # this sends a 400 error if we don't get anything

    merged_list = [(given_condition_names[i], given_condition_values[i]) for i in range(0, len(given_condition_names))]

    for sequence in given_sequence_names:
        experiments = get_experiments_with_sequence(sequence, mydb)

        for experiment_id in experiments:

            conditions = get_condtions_with_experiment_id(experiment_id[0], mydb)
            measurements = get_measurements_with_experiment_id(experiment_id[0], mydb)

            if len(set(merged_list).intersection(set(conditions))) > 0:
                experiment_request_object = {
                    "experiment_id": experiment_id[0],
                    "measurements": measurements,
                    "conditions": conditions
                }
                return_list.append(experiment_request_object)

    request_object = {
        "experiments": return_list
    }

    return request_object
