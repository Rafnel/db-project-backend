import mysql
from mysql.connector import Error
from flask import Blueprint, request

# register this python file as part of the flask app.
from sql.connection import establish_db_connection
from sql.input import input_new_sequence, input_new_measurement_with_experiment
from sql.retrieval import get_measurement, get_measurement_with_experiment
from utils.type_checking import check_value_follows_domain

field_input = Blueprint("field_input", __name__)


@field_input.route("/measurement-values", methods=["POST"])
def enter_new_measurement_values():
    experiment_id = request.get_json()["experiment_id"]
    measurements = request.get_json()["measurements"]

    db = establish_db_connection()

    status_object = {
        "entered": [],
        "failed": []
    }

    # for each measurement, add a new measurement with experiment to the db.
    for measurement in measurements:
        # check if this measurement has already been recorded for this experiment.
        recorded = get_measurement_with_experiment(measurement["name"], experiment_id, db)
        if recorded is not None:
            # there is already a measurement for this experiment with this same name. Don't add.
            status_object["failed"].append("Measurement " + measurement["name"] + " with value " + measurement["value"] + " could not be added because this measurement is already recorded for this experiment.")
        else:
            # ensure that the measurement value follows its domain.
            msmt = get_measurement(measurement["name"], db)
            # Ensure that the value is of the right domain.
            follows_domain = check_value_follows_domain(measurement["value"], msmt[1])

            if not follows_domain:
                # don't add, write it in the return object.
                domain = ""
                if msmt[1] == "integer":
                    domain = "Whole Number"
                elif msmt[1] == "floating_point":
                    domain = "Decimal Number"
                elif msmt[1] == "string":
                    domain = "Text"
                elif msmt[1] == "boolean":
                    domain = "True / False"
                status_object["failed"].append({
                        "Measurement": measurement["name"],
                        "Value": measurement["value"],
                        "Reason": "This measurement value does not follow the domain of " + domain + " for this measurement."
                })
            elif follows_domain:
                # input to the db
                success = input_new_measurement_with_experiment(measurement["name"], experiment_id, measurement["value"], db)
                if not success:
                    status_object["failed"].append({
                        "Measurement": measurement["name"],
                        "Value": measurement["value"],
                        "Reason": "There is a measurement of the same name recorded for this experiment already."
                    })
                else:
                    status_object["entered"].append("Measurement: " + measurement["name"] + ", Value: " + measurement["value"])

    return status_object


@field_input.route("/condition_fields", methods=["POST"])
def input_new_conditon_fields():
    condition_name = request.get_json()["condition_name"]
    condition_domain = request.get_json()["condition_domain"]

    # make this object just for returning purposes.
    request_object = {
        "condition_name": condition_name,
        "condition_domain": condition_domain
    }

    # perform the insertion
    db = establish_db_connection()
    status = input_new_condition_fields(condition_name, condition_domain, db)

    # set the response code to 201 for "created successfully", or set it to 400 for failed insertion
    response_code = 201
    if status is False:
        response_code = 400

    return request_object, response_code


def input_new_condition_fields(condition_name, condition_domain, mydb):
    if condition_name == "":
        return False
    if condition_domain == "":
        return False

    try:
        sql = "INSERT INTO `Experiment_Conditions` (`condition_name`, `domain`) VALUES (%s, %s)"
        mydb.cursor().execute(sql, [condition_name, condition_domain])
        mydb.commit()
        return True
    except mysql.connector.Error as e:
        return False


@field_input.route("/measurement_fields", methods=["POST"])
def input_new_measurement():
    measurement_name = request.get_json()["measurement_name"]
    measurement_domain = request.get_json()["measurement_domain"]

    # make this object just for returning purposes.
    request_object = {
        "measurement_name": measurement_name,
        "measurement_domain": measurement_domain
    }

    # perform the insertion
    db = establish_db_connection()
    status = input_new_measurement_fields(measurement_name, measurement_domain, db)

    # set the response code to 201 for "created successfully", or set it to 400 for failed insertion
    response_code = 201
    if status is False:
        response_code = 400

    return request_object, response_code


def input_new_measurement_fields(measurement_name, measurement_domain, db):
    if measurement_name == "":
        return False
    if measurement_domain == "":
        return False

    try:
        sql = "INSERT INTO `Measurements` (`measurement_name`, `measurement_domain`) VALUES (%s, %s)"
        db.cursor().execute(sql, [measurement_name, measurement_domain])
        db.commit()
        return True
    except mysql.connector.Error as e:
        return False
