from flask import Blueprint, request

# register this python file as part of the flask app.
from sql.connection import establish_db_connection
from sql.input import input_new_sequence, input_new_experiment, input_new_experiment_with_condition, \
    input_new_measurement_with_experiment
from sql.retrieval import get_sequence, get_condition, get_experiment_with_conditions, get_measurement
from utils.type_checking import check_value_follows_domain

manual_input = Blueprint("manual_input", __name__)


@manual_input.route("/sequence", methods=["POST"])
def input_sequence():
    sequence_name = request.get_json()["sequence_name"]
    sequence_location = request.get_json()["sequence_location"]
    sequence_description = request.get_json()["sequence_description"]

    # make this object just for returning purposes.
    request_object = {
        "sequence_name": sequence_name,
        "sequence_location": sequence_location,
        "sequence_description": sequence_description
    }

    # perform the insertion
    db = establish_db_connection()
    status = input_new_sequence(sequence_name, sequence_location, sequence_description, db)

    # set the response code to 201 for "created successfully", or set it to 400 for failed insertion
    response_code = 201
    if status is False:
        response_code = 400

    return request_object, response_code


# assumption: all conditions and measurements sent to this already exist, since they are checkboxes on the frontend.
@manual_input.route("/experiment", methods=["POST"])
def enter_new_experiment():
    # get all of the arguments sent to the function
    request_data = request.get_json()
    sequence_name = request_data["sequence_name"]
    sequence_location = request_data["sequence_location"]
    sequence_description = request_data["sequence_description"]

    condition_names = request_data["condition_names"]
    condition_values = request_data["condition_values"]

    measurement_names = request_data["measurement_names"]
    measurement_values = request_data["measurement_values"]

    mydb = establish_db_connection()
    return_object = {
        "text": "",
        "success": True
    }
    # 0.5. check that there is an equal number of condition names and condition values.
    if len(condition_names) != len(condition_values):
        return_object["success"] = False
        return_object["text"] = "You should have the same number of conditions and condition values"

        return return_object
    if len(measurement_names) != len(measurement_values):
        return_object["success"] = False
        return_object["text"] = "You should have the same number of measurements and measurement values"

        return return_object

    # 0.75. check that the sequence_name doesn't have underscore.
    if "_" in sequence_name:
        return_object["success"] = False
        return_object["text"] = "Sequence " + sequence_name + " has an underscore. Underscores are not allowed."

        return return_object

    # 1. check if the sequence exists.
    sequence = get_sequence(sequence_name, mydb)
    sequence_exists = True
    if sequence is None:
        sequence_exists = False

    # 2. Ensure that each condition value matches its domain
    # AND 3. Ensure that this set of (sequence, conditions+values) does not already exist as an experiment.
    i = 0
    all_exist_already = True
    while i < len(condition_names):
        # Ensure that the condition value does not contain underscore
        if "_" in condition_values[i]:
            return_object["success"] = False
            return_object["text"] = "Condition " + condition_names[i] + " can not have value " + condition_values[i] + \
                                    " because it has an underscore."

            return return_object

        condition = get_condition(condition_names[i], mydb)
        # Ensure that the value is of the right domain.
        follows_domain = check_value_follows_domain(condition_values[i], condition[1])
        if not follows_domain:
            # the value doesn't follow the domain.. so we can't enter this experiment.
            return_object["success"] = False
            return_object["text"] = "Condition " + condition_names[i] + " with value " + condition_values[i] + \
                                    " does not follow domain " + condition[1]

            return return_object
        # if this condition is a boolean, change it to true or false for consistency.
        if condition[1] == "boolean":
            if condition_values[i].lower().startswith("t") or condition_values[i] == "1":
                condition_values[i] = "true"
            else:
                condition_values[i] = "false"

        # now see if this exact (sequence, condition, condition_value) exist already.
        experiment_with_conditions = get_experiment_with_conditions(condition_names[i],
                                                                    condition_values[i], mydb)
        if experiment_with_conditions is None:
            all_exist_already = False
        else:
            # there are some exp_with_conds with same cond_name and cond_value, check if same seq_name.
            one_matches = False
            for exp_with_cond in experiment_with_conditions:
                split_list = exp_with_cond[1].split("_")
                if split_list[0] == sequence_name:
                    one_matches = True
            if not one_matches:
                all_exist_already = False

        i = i + 1

    if all_exist_already:
        # an experiment already exists with this exact set of (sequence_name, conditions)
        return_object["text"] = "An experiment already exists with this exact set of (sequence, conditions, " \
                                "condition_values) "
        return_object["success"] = False

        return return_object

    # 4. ensure that each measurement value also follows the domain of its corresponding measurement.
    i = 0
    while i < len(measurement_names):
        # Ensure that the measurement value does not contain underscore
        if "_" in measurement_values[i]:
            return_object["success"] = False
            return_object["text"] = "Measurement " + measurement_names[i] + " can not have value " + measurement_values[i] + \
                                    " because it has an underscore."

            return return_object
        measurement_from_db = get_measurement(measurement_names[i], mydb)
        # check that the value corresponds with the domain for the measurement.
        follows_domain = check_value_follows_domain(measurement_values[i], measurement_from_db[1])
        if not follows_domain:
            # the value doesn't follow the domain.. so we can't enter this experiment.
            return_object["success"] = False
            return_object["text"] = "Measurement " + measurement_names[i] + " with value " + measurement_values[i] + \
                                    " does not follow domain " + measurement_from_db[i]

            return return_object
        i = i + 1

    # OKAY: we made it through all the checks. start the process of adding to the database.
    # 1. add the sequence to the database if necessary.
    if not sequence_exists:
        input_new_sequence(sequence_name, sequence_location, sequence_description, mydb)

    # 2. construct the experiment id (seqname_cond1name_cond1value_cond2name_cond2value...etc) then add the new
    # experiment to the database
    experiment_id = sequence_name
    i = 0
    while i < len(condition_names):
        experiment_id += "_" + condition_names[i] + "_" + condition_values[i]
        i = i + 1

    input_new_experiment(experiment_id, sequence_name, mydb)

    # 3. for each condition with value, add to the database
    i = 0
    while i < len(condition_names):
        input_new_experiment_with_condition(condition_names[i], experiment_id, condition_values[i], mydb)
        i = i + 1

    # 4. for each measurement with value, add to the database.
    i = 0
    while i < len(measurement_names):
        input_new_measurement_with_experiment(measurement_names[i], experiment_id, measurement_values[i], mydb)
        i = i + 1

    # finished!
    return_object["success"] = True
    return_object["text"] = "Experiment " + experiment_id + " has been successfully entered into the database!"

    return return_object
