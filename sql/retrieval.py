import mysql
from mysql.connector import Error


def get_sequence(sequence_name, mydb):
    try:
        sql = "SELECT * FROM `Sequences` WHERE `name` = %s"
        cursor = mydb.cursor()
        cursor.execute(sql, (sequence_name,))
        return cursor.fetchone()
    except mysql.connector.Error as e:
        print(e)
        return None


def get_condition(condition_name, mydb):
    try:
        sql = "SELECT DISTINCT * FROM `experiment_conditions` WHERE `condition_name` = %s"
        cursor = mydb.cursor()
        cursor.execute(sql, (condition_name,))
        return cursor.fetchone()
    except mysql.connector.Error as e:
        print(e)
        return None


def get_measurement(measurement_name, mydb):
    try:
        sql = "SELECT DISTINCT * FROM `measurements` WHERE `measurement_name` = %s"
        cursor = mydb.cursor()
        cursor.execute(sql, (measurement_name,))
        return cursor.fetchone()
    except mysql.connector.Error as e:
        print(e)
        return None


def get_all_conditions(mydb):
    sql = "SELECT DISTINCT * FROM `experiment_conditions`"
    cursor = mydb.cursor()
    cursor.execute(sql)
    return cursor.fetchall()


def get_measurement_with_experiment(experiment_id, measurement_name, mydb):
    try:
        sql = "SELECT * FROM `measurement_with_experiment` WHERE `measurement_name` = %s AND `experiment_id` = %s"
        cursor = mydb.cursor()
        cursor.execute(sql, (measurement_name, experiment_id,))
        return cursor.fetchone()
    except mysql.connector.Error as e:
        print(e)
        return None


def get_all_sequences(mydb):
    sql = "SELECT DISTINCT * FROM `sequences`"
    cursor = mydb.cursor()
    cursor.execute(sql)
    return cursor.fetchall()


def get_all_measurements(mydb):
    sql = "SELECT DISTINCT * FROM `measurements`"
    cursor = mydb.cursor()
    cursor.execute(sql)
    return cursor.fetchall()


def get_all_experiments(mydb):
    sql = "select experiment_id from `experiments`"
    cursor = mydb.cursor()
    cursor.execute(sql)
    return cursor.fetchall()


def get_condition_values_with_name(condition_name, mydb):
    sql = "SELECT DISTINCT * FROM `experiment_with_condition` WHERE `condition_name` = %s"
    cursor = mydb.cursor()
    cursor.execute(sql, (condition_name,))
    return cursor.fetchall()


def get_experiment_with_conditions(condition_name, condition_value, mydb):
    sql = "SELECT * FROM `experiment_with_condition` WHERE `condition_name` = %s AND " \
          "condition_value = %s"

    cursor = mydb.cursor()
    cursor.execute(sql, (condition_name, condition_value,))
    return cursor.fetchall()
