
def createAllTables(mydb):
    dropSequencesTable = "DROP TABLE IF EXISTS `Sequences`"
    dropMeasurementsTable = "DROP TABLE IF EXISTS `Measurements`"
    dropExperiment_With_ConditionTable = "DROP TABLE IF EXISTS `Experiment_With_Condition`"
    dropExperiment_ConditionsTable = "DROP TABLE IF EXISTS `Experiment_Conditions`"
    dropExperimentsTable = "DROP TABLE IF EXISTS `Experiments`"

    createSequencesTable = "CREATE TABLE IF NOT EXISTS `Sequences` (" \
                           "`name` varchar(255) NOT NULL DEFAULT ''," \
                           "`filename` varchar(255) DEFAULT ''," \
                           "`description` varchar(255) DEFAULT ''," \
                           "PRIMARY KEY (`name`))"

    createExperimentsTable = "CREATE TABLE IF NOT EXISTS `Experiments` (" \
                             "`experiment_id` varchar(255) NOT NULL," \
                             "`sequence_name` varchar(16) NOT NULL DEFAULT ''," \
                             "PRIMARY KEY (`experiment_id`)," \
                             "FOREIGN KEY (`sequence_name`) " \
                             "REFERENCES `sequences` (`name`))"

    createExperiment_ConditionsTable = "CREATE TABLE IF NOT EXISTS `Experiment_Conditions` (" \
                                       "`condition_name` varchar(255) NOT NULL DEFAULT ''," \
                                       "`domain` varchar(255) NOT NULL DEFAULT ''," \
                                       "PRIMARY KEY (`condition_name`))"

    createMeasurementsTable = "CREATE TABLE IF NOT EXISTS `Measurements` (" \
                              "`measurement_name` varchar(255) NOT NULL DEFAULT ''," \
                              "`measurement_domain` varchar(255) NOT NULL DEFAULT ''," \
                              "PRIMARY KEY (`measurement_name`))"

    createExperiment_With_ConditionTable = "CREATE TABLE IF NOT EXISTS `Experiment_With_Condition` (" \
                                           "`condition_name` varchar(255) NOT NULL DEFAULT ''," \
                                           "`experiment_id` varchar(255) NOT NULL," \
                                           "`condition_value` varchar(255) NOT NULL," \
                                           "PRIMARY KEY (`condition_name`,`experiment_id`)," \
                                           "FOREIGN KEY (`experiment_id`) " \
                                           "REFERENCES `experiments` (`experiment_id`)," \
                                           "FOREIGN KEY (`condition_name`) " \
                                           "REFERENCES `experiment_conditions` (`condition_name`))"
    create_measurement_with_experiment_table = "CREATE TABLE IF NOT EXISTS `measurement_with_experiment` (" \
                                               "`measurement_name` varchar(255) NOT NULL DEFAULT ''," \
                                               "`experiment_id` varchar(255) NOT NULL," \
                                               "`measurement_value` varchar(255) NOT NULL DEFAULT ''," \
                                               "PRIMARY KEY (`measurement_name`,`experiment_id`)," \
                                               "FOREIGN KEY (`experiment_id`) " \
                                               "REFERENCES `experiments` (`experiment_id`)," \
                                               "FOREIGN KEY (`measurement_name`) " \
                                               "REFERENCES `measurements` (`measurement_name`))"
    try:
        mycursor = mydb.cursor()
        # Drop Tables if Exist
        # mycursor.execute(dropSequencesTable)
        # mycursor.execute(dropMeasurementsTable)
        # mycursor.execute(dropExperiment_With_ConditionTable)
        # mycursor.execute(dropExperiment_ConditionsTable)
        # mycursor.execute(dropExperimentsTable)
        # mydb.commit()
        # Create Tables
        mycursor.execute(createSequencesTable)
        mycursor.execute(createExperimentsTable)
        mycursor.execute(createExperiment_ConditionsTable)
        mycursor.execute(createExperiment_With_ConditionTable)
        mycursor.execute(createMeasurementsTable)
        mycursor.execute(create_measurement_with_experiment_table)
        mydb.commit()
    finally:
        mycursor.close()