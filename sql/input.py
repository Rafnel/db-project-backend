import mysql
from mysql.connector import Error


def input_new_sequence(sequence_name, sequence_location, sequence_description, mydb):
    try:
        sql = "INSERT INTO `Sequences` (`name`, `filename`, `description`) VALUES (%s, %s, %s)"
        mydb.cursor().execute(sql, (sequence_name, sequence_location, sequence_description,))
        mydb.commit()
        return True
    except mysql.connector.Error as e:
        print(e)
        return False


def input_new_experiment(experiment, sequence_name, mydb):
    try:
        sql = "INSERT INTO `experiments` (`experiment_id`, `sequence_name`) VALUES (%s, %s)"
        cursor = mydb.cursor()
        cursor.execute(sql, (experiment, sequence_name,))
        mydb.commit()

        # return the id of the new experiment.
        return cursor.lastrowid
    except mysql.connector.Error as e:
        print(e)
        return False


def input_new_experiment_with_condition(condition_name, experiment_id, condition_value, mydb):
    try:
        sql = "INSERT INTO `experiment_with_condition` (`condition_name`, `experiment_id`, `condition_value`) VALUES " \
              "(%s, %s, %s) "
        cursor = mydb.cursor()
        cursor.execute(sql, (condition_name, experiment_id, condition_value,))
        mydb.commit()
        return True
    except mysql.connector.Error as e:
        print(e)
        return False


def input_new_measurement_with_experiment(measurement_name, experiment_id, measurement_value, mydb):
    try:
        sql = "INSERT INTO `measurement_with_experiment` (`measurement_name`, `experiment_id`, `measurement_value`) " \
              "VALUES (%s, %s, %s)"
        cursor = mydb.cursor()
        cursor.execute(sql, (measurement_name, experiment_id, measurement_value,))
        mydb.commit()
        return True
    except mysql.connector.Error as e:
        print(e)
        return False


def input_new_measurement_with_experiment_different_return(measurement_name, experiment_id, measurement_value, mydb):
    try:
        sql = "INSERT INTO `measurement_with_experiment` (`measurement_name`, `experiment_id`, `measurement_value`) " \
              "VALUES (%s, %s, %s)"
        cursor = mydb.cursor()
        cursor.execute(sql, (measurement_name, experiment_id, measurement_value,))
        mydb.commit()
        return {"status": True}
    except mysql.connector.Error as e:
        return {
            "status": False,
            "message": str(e)
        }
