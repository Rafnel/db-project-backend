import mysql
from mysql.connector import Error


def get_experiments_with_sequence(sequence_name, mydb):
    cursor = mydb.cursor()
    sql_select_query = "select experiment_id from experiments where sequence_name = %s"
    cursor.execute(sql_select_query, (sequence_name,))
    records = cursor.fetchall()
    return records


def get_condtions_with_experiment_id(experiment_id, mydb):
    cursor = mydb.cursor()
    sql_select_query = "select condition_name, condition_value from Experiment_With_Condition where experiment_id = %s"
    cursor.execute(sql_select_query, (experiment_id,))
    records = cursor.fetchall()
    return records


def get_measurements_with_experiment_id(experiment_id, mydb):
    cursor = mydb.cursor()
    sql_select_query = "select measurement_name, measurement_value from  measurement_with_experiment where experiment_id = %s"
    cursor.execute(sql_select_query, (experiment_id,))
    records = cursor.fetchall()
    return records